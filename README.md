# Spotify Release Tracker
Dashboard to track the latest **Spotify album releases**.

The porpuse of this project is to cover **Playtomic's code challenge**, while giving a valuable tool.

See demo running on **[Netlify](https://spotify-release-tracker.netlify.app/)**

## Setting up the project
A Spotify Client ID is needed to run this app:

- Get one by signing up an app on [Spotify for developers](https://developer.spotify.com/documentation/general/guides/app-settings/)
- Put the client ID in an environment variable called REACT_APP_SPOTIFY_CLIENT_ID
>To achieve this step, you might want to rename the file .env.sample to .env and copy the client id you just generated.

Spotify has a whitelist of URLs that can perform the login, make sure to include yours.

Note for Juanjo and other Playtomic's mates: I can provide you my spotify client id to make things esier, you'll just need to set the environment var and run the project on `http://localhost:3000`

Or see the [deployed version ](https://spotify-release-tracker.netlify.app/)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Libraries and tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). 
Relevant packages are:

- typescript
- react-redux
- react-spotify-aut
- react-bootstrap
- react-router
- @reduxjs/toolkit

I had focus on the use of thunks and hooks, and making use of strict typing.

**Styles:** SASS and CSS modules.
**Formatting:** ESLint and prettier.
**CI/CD:** Very simple implementation on Netlify.

## Structure
```
/src
  /components - contains the react components
  /services - API connection layer
  /store - redux layer
  /styles - reusable styles and variables
```

## Routes
```
/
/login
/dashboard
/dashboard/settings
```

All routes are protected (except login)

## Further improvements
- Improve the UI/UX
    - Add sidebar to mobile devices by implementing toggle
- Implement refresh token
- Add more unit tests, specially tests for [redux](https://redux.js.org/recipes/writing-tests)
    - Fix test warning caused by not having the client id required from react-spotify-auth
- Improve settings page
- Redirect to previous path on login
- Add internationalization e.g. [react-i18Next](https://react.i18next.com/)
- Add 404 page
- Add a music player
- Albums pagination

