import React from 'react'
import { BrowserRouter, Redirect, Route } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import PrivateRoute from './components/PrivateRoute/PrivateRoute'
import { Login } from './components/Login/Login'
import './App.scss'
import { Drawer } from './components/Drawer/Drawer'
import { setAuthToken } from './store/app/appSlice'

export const getCookie = (key: string): string => {
  const b = document.cookie.match('(^|;)\\s*' + key + '\\s*=\\s*([^;]+)')
  return b?.pop() || ''
}

export const copyTokenToRedux = (): void => {
  const token = getCookie('spotifyAuthToken')
  if (token) {
    const dispatch = useDispatch()
    dispatch(setAuthToken(token))
  }
}

function App() {
  copyTokenToRedux()
  return (
    <div className="App">
      {
        <BrowserRouter>
          <Route path="/login">
            <Login />
          </Route>
          <PrivateRoute path="/dashboard" component={Drawer} />
          <Route path="/" exact>
            <Redirect to="login" />
          </Route>
        </BrowserRouter>
      }
    </div>
  )
}

export default App
