import React from 'react'
import { Route, Redirect, RouteProps } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { RootState } from '../../store/rootReducer'

interface IProps extends RouteProps {
  component: React.ComponentType
}
const PrivateRoute = ({ component: Component, ...rest }: IProps) => {
  const isLoggedIn = useSelector((state: RootState) => state.app.token)
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isLoggedIn ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
