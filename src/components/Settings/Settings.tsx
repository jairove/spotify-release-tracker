import React from 'react'
import CountrySelector from './CountrySelector/CountrySelector'

const Settings = (): JSX.Element => {
  return (
    <section>
      <h1 className="font-weight-light mb-4">Settings</h1>
      <CountrySelector />
      <p className="mt-5">Go to the dashboard to see the applied changes.</p>
    </section>
  )
}

export default Settings
