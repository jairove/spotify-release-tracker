import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown'
import DropdownButton from 'react-bootstrap/DropdownButton'
import { useDispatch, useSelector } from 'react-redux'
import { setCountry } from '../../../store/app/appSlice'
import { Country } from '../../../store/app/types'
import { RootState } from '../../../store/rootReducer'

// Country codes must be ISO 3166-1 alpha-2
const COUNTRIES: Country[] = [
  { name: 'Global', isoCode: '' },
  { name: 'France', isoCode: 'FR' },
  { name: 'Germany', isoCode: 'DE' },
  { name: 'Italy', isoCode: 'IT' },
  { name: 'Portugal', isoCode: 'PT' },
  { name: 'Spain', isoCode: 'ES' },
  { name: 'United Kingdom', isoCode: 'GB' },
  { name: 'United States', isoCode: 'US' }
]

export const CountrySelector = () => {
  const dispatch = useDispatch()
  const selectedCountry = useSelector((state: RootState) => state.app.country)
  return (
    <DropdownButton id="country-selector" variant="secondary" title="Select country">
      {COUNTRIES.map((country: Country) => (
        <Dropdown.Item
          eventKey={country.isoCode}
          onClick={() => dispatch(setCountry(country))}
          active={country.isoCode === selectedCountry.isoCode}
        >
          {country.name}
        </Dropdown.Item>
      ))}
    </DropdownButton>
  )
}

export default CountrySelector
