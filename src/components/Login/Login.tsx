import React from 'react'
import { SpotifyAuth, Scopes } from 'react-spotify-auth'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router'
import 'react-spotify-auth/dist/index.css'
import styles from './Login.styles.module.scss'
import { setAuthToken } from '../../store/app/appSlice'
import { RootState } from '../../store/rootReducer'

export const Login = (): JSX.Element => {
  const dispatch = useDispatch()
  const onLogin = (token: string) => {
    dispatch(setAuthToken(token))
    return <Redirect to="/dashboard" />
  }

  const isAlreadyLoggedIn = useSelector((state: RootState) => state.app.token)
  if (isAlreadyLoggedIn) return <Redirect to="/dashboard" />

  return (
    <div className={styles.loginContainer}>
      <div className={styles.loginForm}>
        <h1>Spotify Release Tracker</h1>
        <h4 className="font-weight-light">Check the latest album releases on Spotify.</h4>
        <SpotifyAuth
          redirectUri={window.location.href}
          clientID={process.env.REACT_APP_SPOTIFY_CLIENT_ID}
          scopes={[Scopes.userReadPrivate, Scopes.userReadEmail]}
          onAccessToken={onLogin}
        />
      </div>
    </div>
  )
}
