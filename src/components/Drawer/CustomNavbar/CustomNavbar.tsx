import React from 'react'
import { Navbar } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { refreshProfile } from '../../../store/app/appSlice'
import { RootState } from '../../../store/rootReducer'

const CustomNavbar = () => {
  const dispatch = useDispatch()
  React.useEffect(() => {
    dispatch(refreshProfile())
  }, [dispatch])

  const profile = useSelector((state: RootState) => state.app.profile)

  return (
    <Navbar>
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Text>
          {profile ? (
            <span>
              Signed in as:{' '}
              <a href={profile?.external_urls.spotify}>{profile?.display_name}</a>
            </span>
          ) : (
            ''
          )}
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  )
}
export default CustomNavbar
