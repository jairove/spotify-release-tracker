import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { Switch } from 'react-router-dom'
import PrivateRoute from '../PrivateRoute/PrivateRoute'
import Dashboard from '../Dashboard/Dashboard'
import Settings from '../Settings/Settings'
import Sidebar from './Sidebar/Sidebar'
import CustomNavbar from './CustomNavbar/CustomNavbar'
import styles from './Drawer.module.scss'

export const Drawer = (): JSX.Element => {
  return (
    <div>
      <CustomNavbar />
      <Container fluid>
        <Row>
          <Col md={2} id={styles.sidebarWrapper} className="d-none d-md-block">
            <Sidebar />
          </Col>
          <Col md={10} sm={12} id={styles.contentWrapper}>
            <Switch>
              <PrivateRoute path="/dashboard" component={Dashboard} exact />
              <PrivateRoute path="/dashboard/settings" component={Settings} exact />
            </Switch>
          </Col>
        </Row>
      </Container>
    </div>
  )
}
