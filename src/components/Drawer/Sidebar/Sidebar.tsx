import React from 'react'
import { Nav } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { withRouter } from 'react-router'
import styles from './Sidebar.module.scss'

const MENU_ITEMS = [
  {
    name: 'Dashboard',
    path: '/dashboard'
  },
  {
    name: 'Settings',
    path: '/dashboard/settings'
  }
]

const Sidebar = () => {
  return (
    <Nav className={`col-2 d-none d-md-block bg-dark ${styles.sidebar}`} variant="pills">
      {MENU_ITEMS.map((item) => (
        <Nav.Item>
          <LinkContainer to={item.path} exact>
            <Nav.Link className={styles.link} eventKey={item.name}>
              {item.name}
            </Nav.Link>
          </LinkContainer>
        </Nav.Item>
      ))}
    </Nav>
  )
}
export default withRouter(Sidebar)
