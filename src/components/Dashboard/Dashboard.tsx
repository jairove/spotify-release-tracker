import React from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import Spinner from 'react-bootstrap/Spinner'
import { RootState } from '../../store/rootReducer'
import { refreshAlbums } from '../../store/albums/albumSlice'
import { Album } from '../../store/albums/types'
import { AlbumCard } from './AlbumCard/AlbumCard'

export const Dashboard = (): JSX.Element => {
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch(refreshAlbums())
  }, [dispatch])

  const albumsState = useSelector((state: RootState) => state.albums)
  const countryName = useSelector((state: RootState) => state.app.country.name)

  return (
    <section>
      <h1 className="font-weight-light mb-4">{`${countryName} Latest Releases`}</h1>
      <div className="d-flex flex-direction-column flex-wrap justify-content-center">
        {albumsState.isLoading ? (
          <Spinner animation="grow" className="mt-5" />
        ) : albumsState.error ? (
          <span>There was an error loading the albums</span>
        ) : (
          albumsState.items.map((album: Album, index: number) => (
            <AlbumCard album={album} key={index} />
          ))
        )}
      </div>
    </section>
  )
}

export default Dashboard
