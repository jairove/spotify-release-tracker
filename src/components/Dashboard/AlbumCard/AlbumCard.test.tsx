import React from 'react'
import { render, screen } from '@testing-library/react'
import { AlbumCard } from './AlbumCard'
import { Album } from '../../../store/albums/types'

const mockedAlbum: Album = {
  id: '1',
  name: 'Dark Side of the Moon',
  uri: 'http://',
  artists: [{ id: '432', name: 'Pink Floyd' }],
  images: [
    { height: 400, width: 400, url: '' },
    { height: 200, width: 200, url: '' }
  ]
}

test('renders AlbumCard with a faked album', () => {
  render(<AlbumCard album={mockedAlbum} />)
  const titleElement = screen.getByText(/Dark Side of the Moon/i)
  expect(titleElement).toBeInTheDocument()
  const artistElement = screen.getByText(/Pink Floyd/i)
  expect(artistElement).toBeInTheDocument()
})
