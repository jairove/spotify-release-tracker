import React from 'react'
import Card from 'react-bootstrap/Card'
import { Album, Artist } from '../../../store/albums/types'
import styles from './AlbumCard.module.scss'

export interface AlbumCardProps {
  album: Album
}
export const AlbumCard = (props: AlbumCardProps): JSX.Element => {
  const { album } = props
  return (
    <Card className={styles.card}>
      <Card.Img variant="top" src={album.images[0].url} />
      <Card.Body>
        <Card.Title>
          <a href={album.uri}>{album.name}</a>
        </Card.Title>
        <Card.Text>{album.artists.map((artist: Artist) => artist.name)}</Card.Text>
      </Card.Body>
    </Card>
  )
}
