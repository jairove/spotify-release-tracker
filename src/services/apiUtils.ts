interface HttpResponse<T> extends Response {
  parsedBody?: T
}

export async function getWithAuth<T>(
  request: RequestInfo,
  token: string
): Promise<HttpResponse<T>> {
  const defaultInit: RequestInit = {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json'
    }
  }

  const response: HttpResponse<T> = await fetch(request, defaultInit)
  response.parsedBody = await response.json()
  if (!response.ok) {
    throw new Error(response.statusText)
  }
  return response
}
