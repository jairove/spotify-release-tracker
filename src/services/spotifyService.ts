import { AlbumsResponse } from '../store/albums/types'
import { Profile } from '../store/app/types'
import { getWithAuth } from './apiUtils'

const API_URL = 'https://api.spotify.com/v1'

export const fetchAlbums = async (
  token: string,
  countryCode: string
): Promise<AlbumsResponse | undefined> => {
  let api_path = `${API_URL}/browse/new-releases`
  if (countryCode) api_path = `${api_path}?country=${countryCode}`

  const response = await getWithAuth<{ albums: AlbumsResponse }>(api_path, token)
  return response.parsedBody?.albums
}

export const fetchProfile = async (token: string): Promise<Profile | undefined> => {
  const response = await getWithAuth<Profile>(`${API_URL}/me`, token)
  return response.parsedBody
}
