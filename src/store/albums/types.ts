export interface Artist {
  id: string
  name: string
}

export interface ImageObject {
  height: number
  width: number
  url: string
}

export interface Album {
  id: string
  name: string
  uri: string
  artists: Artist[]
  images: ImageObject[]
}

export interface AlbumsResponse {
  items: Album[]
  limit: number
  total: number
  next: string
  previous: string
}

export interface AlbumsState {
  items: Album[]
  isLoading: boolean
  error: boolean
}
