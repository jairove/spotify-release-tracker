import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { AppThunk, AppDispatch } from '../store'
import { AlbumsState, AlbumsResponse, Album } from './types'
import { fetchAlbums } from '../../services/spotifyService'

const initialState: AlbumsState = {
  items: [],
  isLoading: false,
  error: false
}

const albumSlice = createSlice({
  name: 'albums',
  initialState,
  reducers: {
    fecthAlbumsSuccess(state, action: PayloadAction<Album[]>) {
      return { ...state, isLoading: false, items: action.payload, error: false }
    },
    fecthAlbumsFailed(state) {
      return { ...state, isLoading: false, error: true }
    },
    setIsLoading(state, action: PayloadAction<boolean>) {
      return { ...state, isLoading: action.payload }
    }
  }
})

export const refreshAlbums = (): AppThunk => async (dispatch: AppDispatch, getState) => {
  try {
    const state = getState()
    dispatch(albumSlice.actions.setIsLoading(true))
    const response: AlbumsResponse | undefined = await fetchAlbums(
      state.app.token,
      state.app.country.isoCode
    )
    if (response) {
      const albums = response.items
      dispatch(albumSlice.actions.fecthAlbumsSuccess(albums))
    } else throw new Error()
  } catch (error) {
    dispatch(albumSlice.actions.fecthAlbumsFailed())
  }
}

export default albumSlice.reducer
