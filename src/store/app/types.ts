export interface AppState {
  token: string
  profile?: Profile
  error: boolean
  country: Country
}

export interface Profile {
  display_name: string
  external_urls: {
    spotify: string
  }
}

export interface Country {
  name: string
  isoCode: string
}
