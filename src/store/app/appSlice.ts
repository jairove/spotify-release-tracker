import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AppDispatch, AppThunk } from '../store'
import { AppState, Profile, Country } from './types'
import { fetchProfile } from '../../services/spotifyService'

const initialState: AppState = {
  token: '',
  profile: undefined,
  error: false,
  country: { name: 'Global', isoCode: '' }
}

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setAuthToken(state, action: PayloadAction<string>) {
      return { ...state, token: action.payload }
    },
    fetchProfileSuccess(state, action: PayloadAction<Profile>) {
      return { ...state, profile: action.payload }
    },
    fetchProfileFailed(state) {
      return { ...state, error: true }
    },
    setCountry(state, action: PayloadAction<Country>) {
      return { ...state, country: action.payload }
    }
  }
})

export const refreshProfile = (): AppThunk => async (dispatch: AppDispatch, getState) => {
  try {
    const state = getState()
    const profile: Profile | undefined = await fetchProfile(state.app.token)
    if (profile) {
      dispatch(appSlice.actions.fetchProfileSuccess(profile))
    } else throw new Error()
  } catch (error) {
    dispatch(appSlice.actions.fetchProfileFailed())
  }
}

export const setCountry = appSlice.actions.setCountry
export const setAuthToken = appSlice.actions.setAuthToken
export default appSlice.reducer
