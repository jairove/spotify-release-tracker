import { combineReducers } from '@reduxjs/toolkit'
import albums from './albums/albumSlice'
import app from './app/appSlice'

const rootReducer = combineReducers({ albums, app })

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
