import React from 'react'
import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import App from './App'
import store from './store/store'

interface WrapperProps {
  component: React.ElementType<any>
}
const Wrapper = ({ component: Component }: WrapperProps) => (
  <Provider store={store}>
    <Component />
  </Provider>
)

test('renders app without an authenticated user', () => {
  render(<Wrapper component={App}></Wrapper>)
  const titleElement = screen.getByText(/Spotify Release Tracker/i)
  expect(titleElement).toBeInTheDocument()
})
